package pullbasic;

import java.util.ArrayList;

public class DialogueImpl extends DialoguePOA{
	
	
	class Member{
		
		private int label;
		private String userName;
		
		public Member( int lab,String userName){
			this.label=lab;
			this.userName=userName;
		}
		
		public int getLabel() {
			return label;
		}
		public void setLabel(int label) {
			this.label = label;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		
	}

	public ArrayList<Member> myMembers = new ArrayList<Member>();
	public static int etiquette=0;
	
	
	@Override
	public void connect(String pseudo) {
		
		Member newMember = new Member(etiquette,pseudo);
		etiquette++;
		myMembers.add(newMember);
		System.out.println(newMember.getUserName()+" a demand� � rejoindre le chat");
		
		
	}

	@Override
	public void disconnect(String pseudo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getClients() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sendMessage(String from, String to, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getMessages(String pseudo) {
		// TODO Auto-generated method stub
		return null;
	}

}
